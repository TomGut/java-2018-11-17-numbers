package pl.codementors;

import java.util.Scanner;

/**
 * app take user input for tab size, checks out which value is min and max and print those out
 * @author Tomek Gutowski
 */

public class Zad2Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //predefined tab used by if/else checkout for tab size
        int[] userTab;
        System.out.println("Podaj rozmiar tablicy");
        //tab size is determined by user input
        int tabSize = scanner.nextInt();
        //when user gives 0 or negative tab index
        if (tabSize <= 0) {
            System.out.println("Podałeś rozmiar tablicy mniejszy od 1");
        //when tab length >= 1
        } else {
            userTab = new int[tabSize];
            //user provide value of every index
            for (int i = 0; i < userTab.length; i++) {
                System.out.println("Podaj wartość indexu nr " + i);
                int indexValue = scanner.nextInt();
                userTab[i] = indexValue;
            }
            //comparison of values in tab starts from index 0
            int min = userTab[0];
            int max = userTab[0];
            //max value
            for (int i = 1; i < userTab.length; i++) {
                //tmp param
                int number = userTab[i];
                if (number > max) {
                    max = userTab[i];
                }
            }
            //min value
            for (int i = 1; i < userTab.length; i++) {
                //tmp param
                int number = userTab[i];
                if (number < min) {
                    min = userTab[i];
                }
            }
            System.out.println("Zadany rozmiar tablicy to " + userTab.length + " indeksów");
            System.out.println("Największa wartość w tab to " + max);
            System.out.println("Najmniejsza wartość w tab to " + min);
        }
    }
}
