package pl.codementors;

import java.util.Scanner;

/**
 * app converse decimal number into binary one - in range of 0 - 15 in decimal
 * @author Tomek Gutowski
 */

public class Zad3Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        //empty tab for loop values
        int[] binaryNumber = new int[5];
        System.out.println("Podaj liczbę całkowitą do zamiany na binarną");
        //user input
        int userNumber = scanner.nextInt();

        //checking if user input covers 0 - 15 value range
        if (userNumber >= 0 && userNumber <= 15) {
            int result = 0;
            //filling out tab with results
            for(int i=1; userNumber > 0 ; i++){
                result = userNumber%2;
                userNumber = userNumber/2;
                binaryNumber[i] = result;
            }

            System.out.println("Zapis binarny");
            //reversed value output
            for (int i=4; userNumber < i; i--){
                System.out.print(binaryNumber[i]);
            }
        //if user input number outside 0 - 15 range
        }else{
            System.out.println("Podano liczbę spoza zakresu 0-15");
        }
    }
}
