package pl.codementors;

import java.util.Scanner;

/**
 * app takes user input of natural numbers, when 42 typed program is terminated
 * @author Tomek Gutowski
 */

public class Zad1Main {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        //When false - while loop stops
        boolean loopIsRunning = true;

        while (loopIsRunning){
            System.out.println("Podaj liczbę całkowitą");
            System.out.println();
            //user gives number at input
            int userNumber = scanner.nextInt();
            switch (userNumber) {
                //when user type 42 loopIsRunning go false and loop stops
                case 42: {
                    System.out.println("Podałeś liczbę " + userNumber + " która kończy pętlę");
                    loopIsRunning = false;
                    break;
                }
                default: {
                    System.out.println("Podałeś " + userNumber);
                    System.out.println();
                }
            }
        }
    }
}
